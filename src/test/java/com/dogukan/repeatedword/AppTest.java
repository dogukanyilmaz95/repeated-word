package com.dogukan.repeatedword;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.ArrayList;

/**
 * Unit test for simple App.
 */
public class AppTest {
    @Test
    public void wordTest() {

        ArrayList<String> words = new ArrayList<String>();
        words.add("apple");
        words.add("apple");
        words.add("pie");
        words.add("red");
        words.add("red");
        words.add("red");

        App app = new App();

        String result = app.SortOfWord(words);


        assertEquals("red", result);

    }
}
