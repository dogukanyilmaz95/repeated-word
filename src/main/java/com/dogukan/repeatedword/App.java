package com.dogukan.repeatedword;

import java.util.ArrayList;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<String>();
        words.add("apple");
        words.add("apple");
        words.add("pie");
        words.add("red");
        words.add("red");
        words.add("red");
        SortOfWord(words);
    }

    public static String SortOfWord(ArrayList words) {
        int repeated = 0;
        String word = "";
        for (int i = 0; i < words.size(); i++) {
            int softRepeated = 0;
            for (int j = 0; j < words.size(); j++) {
                if (words.get(i).equals(words.get(j))) {
                    softRepeated++;
                }
                if (softRepeated > repeated) {
                    repeated = softRepeated;
                    word = words.get(i).toString();
                }
            }
        }
        System.out.println(word);
        return word;
    }
}
